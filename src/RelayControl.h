//
// Created by jh on 09.05.21.
//

#ifndef WORKSPACE_XML_RELAYCONTROL_H
#define WORKSPACE_XML_RELAYCONTROL_H

#endif //WORKSPACE_XML_RELAYCONTROL_H

void launchConfigMode();
void updateTime();
void updateTime(struct tm *ltm);
void handleRoot();
void handleAuto();
void handleOn();
void handleOff();
void handleConfig();
void handleReset();
void handleReboot();
void handleTest();
void handleNotFound();

