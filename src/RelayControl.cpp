#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <TZ.h>
#include <coredecls.h>
#include <Schedule.h>
#include <PolledTimeout.h>

#include <ctime>

#include <sntp.h>

#include <EEPROM.h>
#include <ESP8266HTTPUpdateServer.h>

#include "RelayControl.h"

typedef struct {
    int valid;                        // 71=valid configuration
    char SSID[33];                    // SSID of WiFi
    char password[33];                // Password of WiFi
    char days_0[8];
    char hours_0[25];
    char minutes_0[61];
    char days_1[8];
    char hours_1[25];
    char minutes_1[61];
    char days_2[8];
    char hours_2[25];
    char minutes_2[61];
    char days_3[8];
    char hours_3[25];
    char minutes_3[61];
} configData_t;

configData_t PersistenceStruct;

char buffer[4096];

#define MYTZ TZ_Europe_London

static esp8266::polledTimeout::periodicMs checkInterval(5000);

bool relayOn = true;
bool timedMode = true;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

static time_t now;

uint32_t sntp_startup_delay_MS_rfc_not_less_than_60000() {
    return 60000;
}

char custom_hostname[33];

void connect();

void startServer();

/* ===================================================================== */


const char *getConfig() {
    snprintf(buffer, sizeof(buffer),
             R"({"host":"%s", "SSID":"%s", "days_0":"%s", "days_1":"%s", "days_2":"%s", "days_3":"%s", "hours_0":"%s", "hours_1":"%s", "hours_2":"%s", "hours_3":"%s", "minutes_0":"%s", "minutes_1":"%s", "minutes_2":"%s", "minutes_3":"%s"})",
             custom_hostname,
             PersistenceStruct.SSID,
             PersistenceStruct.days_0,
             PersistenceStruct.days_1,
             PersistenceStruct.days_2,
             PersistenceStruct.days_3,
             PersistenceStruct.hours_0,
             PersistenceStruct.hours_1,
             PersistenceStruct.hours_2,
             PersistenceStruct.hours_3,
             PersistenceStruct.minutes_0,
             PersistenceStruct.minutes_1,
             PersistenceStruct.minutes_2,
             PersistenceStruct.minutes_3
    );

    return buffer;
}

const char *getStatus() {
    if (timedMode) {
        if (relayOn) {
            return R"({"status": 1, "timeMode": 1})";
        } else {
            return R"({"status": 0, "timeMode": 1})";
        }
    } else {
        if (relayOn) {
            return R"({"status": 1, "timeMode": 0})";
        } else {
            return R"({"status": 0, "timeMode": 0})";
        }
    }
}

void setup(void) {
    WiFi.persistent(false);
    uint32_t apiChipId = ESP.getChipId();

    snprintf(custom_hostname, sizeof(custom_hostname), "relay-%d", apiChipId);

    Serial.begin(115200);

    delay(5000);

    pinMode(0, OUTPUT);
    digitalWrite(0, HIGH);

    // Read config
    EEPROM.begin(sizeof(
                         PersistenceStruct
                 ));

    EEPROM.get(0,
               PersistenceStruct
    );

    Serial.println("Booting");
    Serial.println(custom_hostname);
    Serial.println(PersistenceStruct.valid);

    if (PersistenceStruct.valid != 71) {
        PersistenceStruct.valid = 71;
        strcpy(PersistenceStruct.SSID, "");
        strcpy(PersistenceStruct.password, "");
        strcpy(PersistenceStruct.days_0, "0xxxxx0");
        strcpy(PersistenceStruct.hours_0, "xxxxx000xxxxxxxx0000000x");
        strcpy(PersistenceStruct.minutes_0, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        strcpy(PersistenceStruct.days_1, "x00000x");
        strcpy(PersistenceStruct.hours_1, "xxxxx000000000000000000x");
        strcpy(PersistenceStruct.minutes_1, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        strcpy(PersistenceStruct.days_2, "0000000");
        strcpy(PersistenceStruct.hours_2, "000000000000000000000000");
        strcpy(PersistenceStruct.minutes_2, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        strcpy(PersistenceStruct.days_3, "0000000");
        strcpy(PersistenceStruct.hours_3, "000000000000000000000000");
        strcpy(PersistenceStruct.minutes_3, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        launchConfigMode();
        return;
    }

    connect();

}

void newNtpTime() {
    Serial.println("UPDATE_TIME");
}


void connect() {
    WiFi.mode(WIFI_STA);
    WiFi.hostname(custom_hostname);
    WiFi.begin(PersistenceStruct.SSID, PersistenceStruct.password);
    Serial.println("");

    int retryCount = 0;

    // Wait for connection
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.print(".");
        retryCount++;
        if (retryCount == 100) {
            launchConfigMode();
            return;
        }
    }

    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(PersistenceStruct.SSID);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    startServer();

    Serial.println("HTTP server started");

    settimeofday_cb(std::bind(newNtpTime));

    configTime(MYTZ, "pool.ntp.org");

    updateTime();
}

void launchConfigMode() {
    WiFi.mode(WIFI_AP);
    Serial.println("Setting soft-AP configuration ... ");
    Serial.print("Launch Config Mode ");
    Serial.println(WiFi.softAP(custom_hostname, custom_hostname) ? "Ready" : "Failed!");
    startServer();
}

void startServer() {
    server.on("/", std::bind(handleRoot));
    server.on("/2", std::bind(handleAuto));
    server.on("/1", std::bind(handleOn));
    server.on("/0", std::bind(handleOff));
    server.on("/config", std::bind(handleConfig));
    server.on("/reset", std::bind(handleReset));
    server.on("/reboot", std::bind(handleReboot));
    server.on("/test", std::bind(handleTest));
    httpUpdater.setup(&server, "fwupd", "xyiant");
    server.onNotFound(std::bind(handleNotFound));
    server.begin();
}

/* ===================================================================== */

void loop(void) {
    server.handleClient();
    if (checkInterval) {

        now = time(nullptr);
        struct tm *ltm = localtime(&now);

        Serial.printf(
                "%01d %04d/%02d/%02d %02d:%02d %s %s\n",
                ltm->tm_wday,
                ltm->tm_year + 1900,
                ltm->tm_mon,
                ltm->tm_mday,
                ltm->tm_hour,
                ltm->tm_min,
                timedMode ? "IS-TIMED": "IS-NON-TIMED",
                relayOn ? "IS-ON": "IS-OFF"
        );

        if (WiFi.status() != WL_CONNECTED && WiFi.getMode() != WIFI_AP) {
            Serial.println("CONNECTION_LOST");
            connect();
            return;
        }
        if (timedMode) {
            updateTime(ltm);
        }
    }
}


/*===================================================================
                          ACTIONS
  ===================================================================*/


void turnOff() {
    Serial.println("FAN OFF");
    digitalWrite(0, LOW);
    relayOn = false;
}

void turnOn() {
    Serial.println("FAN ON");
    digitalWrite(0, HIGH);
    relayOn = true;
}

/*===================================================================
                          SERVER URLs
  ===================================================================*/

void handleRoot() {
    server.send(200, "application/json", getStatus());
}

void handleNotFound() {
    server.send(404, "text/plain", "Not Found 404");
}

void handleOff() {
    timedMode = false;
    turnOff();
    server.send(200, "application/json", getStatus());
}

void handleOn() {
    timedMode = false;
    turnOn();
    server.send(200, "application/json", getStatus());
}

void handleAuto() {
    timedMode = true;
    updateTime();
    server.send(200, "application/json", getStatus());
}

void handleConfig() {
    if (server.arg("SSID") != "") {
        server.arg("SSID").toCharArray(PersistenceStruct.SSID, 33);
    }
    if (server.arg("password") != "") {
        server.arg("password").toCharArray(PersistenceStruct.password, 33);
    }
    if (server.arg("days_0") != "") {
        server.arg("days_0").toCharArray(PersistenceStruct.days_0, 8);
    }
    if (server.arg("days_1") != "") {
        server.arg("days_1").toCharArray(PersistenceStruct.days_1, 8);
    }
    if (server.arg("days_2") != "") {
        server.arg("days_2").toCharArray(PersistenceStruct.days_2, 8);
    }
    if (server.arg("days_3") != "") {
        server.arg("days_3").toCharArray(PersistenceStruct.days_3, 8);
    }
    if (server.arg("hours_0") != "") {
        server.arg("hours_0").toCharArray(PersistenceStruct.hours_0, 25);
    }
    if (server.arg("hours_1") != "") {
        server.arg("hours_1").toCharArray(PersistenceStruct.hours_1, 25);
    }
    if (server.arg("hours_2") != "") {
        server.arg("hours_2").toCharArray(PersistenceStruct.hours_2, 25);
    }
    if (server.arg("hours_3") != "") {
        server.arg("hours_3").toCharArray(PersistenceStruct.hours_3, 25);
    }
    if (server.arg("minutes_0") != "") {
        server.arg("minutes_0").toCharArray(PersistenceStruct.minutes_0, 61);
    }
    if (server.arg("minutes_1") != "") {
        server.arg("minutes_1").toCharArray(PersistenceStruct.minutes_1, 61);
    }
    if (server.arg("minutes_2") != "") {
        server.arg("minutes_2").toCharArray(PersistenceStruct.minutes_2, 61);
    }
    if (server.arg("minutes_3") != "") {
        server.arg("minutes_3").toCharArray(PersistenceStruct.minutes_3, 61);
    }
    server.send(200, "application/json", getConfig());
}

void handleReboot() {
    Serial.println("Save...");
    EEPROM.put(0, PersistenceStruct);
    EEPROM.commit();
    Serial.println("Reset...");
    ESP.restart();
}

void handleReset() {
    PersistenceStruct.valid = 0;
    handleReboot();
}
void handleTest() {
    server.send(200, "application/json", R"({"status": 2, "timeMode": 0})");
    WiFi.disconnect();
}

void updateTime() {
    now = time(nullptr);
    struct tm *ltm = localtime(&now);
    updateTime(ltm);
}

void updateTime(struct tm *ltm) {
    boolean newStatusOn0 = true;
    newStatusOn0 &= (PersistenceStruct.days_0[ltm->tm_wday] == 'x');
    newStatusOn0 &= (PersistenceStruct.hours_0[ltm->tm_hour] == 'x');
    newStatusOn0 &= (PersistenceStruct.minutes_0[ltm->tm_min] == 'x');

    boolean newStatusOn1 = true;
    newStatusOn1 &= (PersistenceStruct.days_1[ltm->tm_wday] == 'x');
    newStatusOn1 &= (PersistenceStruct.hours_1[ltm->tm_hour] == 'x');
    newStatusOn1 &= (PersistenceStruct.minutes_1[ltm->tm_min] == 'x');

    boolean newStatusOn2 = true;
    newStatusOn2 &= (PersistenceStruct.days_2[ltm->tm_wday] == 'x');
    newStatusOn2 &= (PersistenceStruct.hours_2[ltm->tm_hour] == 'x');
    newStatusOn2 &= (PersistenceStruct.minutes_2[ltm->tm_min] == 'x');

    boolean newStatusOn3 = true;
    newStatusOn3 &= (PersistenceStruct.days_3[ltm->tm_wday] == 'x');
    newStatusOn3 &= (PersistenceStruct.hours_3[ltm->tm_hour] == 'x');
    newStatusOn3 &= (PersistenceStruct.minutes_3[ltm->tm_min] == 'x');

    boolean newStatusOn = newStatusOn0 || newStatusOn1 || newStatusOn2 || newStatusOn3;

    if (newStatusOn && !relayOn) {
        turnOn();
    } else if (!newStatusOn && relayOn) {
        turnOff();
    }
}
